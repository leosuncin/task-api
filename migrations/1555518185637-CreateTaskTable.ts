import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateTaskTable1555518185637 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TYPE "task_priority_enum" AS ENUM(
      'URGENT',
      'HIGHEST',
      'HIGH',
      'NORMAL',
      'LOW',
      'LOWEST'
    )`)
    await queryRunner.query(`CREATE TABLE "task" (
      "task_id" uuid NOT NULL DEFAULT uuid_generate_v4(),
      "title" character varying NOT NULL,
      "description" character varying NULL,
      "date" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
      "from" TIME WITH TIME ZONE NOT NULL,
      "to" TIME WITH TIME ZONE NOT NULL,
      "allDay" boolean NOT NULL DEFAULT false,
      "priority" "task_priority_enum" NOT NULL DEFAULT 'NORMAL',
      "location" character varying NULL,
      "metadata" jsonb NOT NULL DEFAULT '{}',
      CONSTRAINT "task_PK" PRIMARY KEY ("task_id")
    )`)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "task"`)
    await queryRunner.query(`DROP TYPE "task_priority_enum"`)
  }
}
