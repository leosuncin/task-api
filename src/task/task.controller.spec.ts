import { Test, TestingModule } from '@nestjs/testing'
import { TaskController } from './task.controller'
import { NotFoundException } from '@nestjs/common'
import { TaskCreate } from './dto/task-create.dto'
import { TaskPriority } from './task-priority.enum'
import { TaskUpdate } from './dto/task-update.dto'

describe('Task Controller', () => {
  let controller: TaskController
  const MockService = jest.fn().mockImplementation(() => ({
    find() {
      return Promise.resolve([])
    },
    get(id) {
      if (id === '00000000-0000-0000-0000-000000000000') {
        return Promise.reject(new NotFoundException())
      }
      return Promise.resolve({ id })
    },
    create(dto) {
      return Promise.resolve(dto)
    },
    update(id, dto) {
      if (id === '00000000-0000-0000-0000-000000000000') {
        return Promise.reject(new NotFoundException())
      }
      return Promise.resolve({ id, ...dto })
    },
    remove(id) {
      return Promise.resolve({
        raw: [],
        affected: id === '00000000-0000-0000-0000-000000000000' ? 0 : 1,
      })
    },
  }))

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaskController],
      providers: [
        {
          provide: 'TaskService',
          useClass: MockService,
        },
      ],
    }).compile()

    controller = module.get<TaskController>(TaskController)
  })

  afterEach(() => {
    MockService.mockClear()
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should list all the tasks', () => {
    expect(controller.find()).resolves.toEqual([])
  })

  it('should create a new task', () => {
    expect.assertions(1)
    const date = new Date()
    date.setHours(0, 0, 0, 0)
    date.setDate(date.getDate() + 1)
    const newTask: TaskCreate = {
      title: 'Make a sandwich',
      description: '',
      date,
      from: '10:00:00-06:00',
      to: '11:00:00-06:00',
      allDay: false,
      priority: TaskPriority.NORMAL,
      location: 'Kitchen',
    }

    expect(controller.create(newTask)).resolves.toBeDefined()
  })

  it('should get one task', () => {
    expect.assertions(1)
    const id = '00000000-0000-0000-0000-000000000001'

    expect(controller.get(id)).resolves.toBeDefined()
  })

  it('should fail to get one task', () => {
    expect.assertions(1)
    const id = '00000000-0000-0000-0000-000000000000'

    expect(controller.get(id)).rejects.toBeDefined()
  })

  it('should update one task', () => {
    expect.assertions(1)
    const id = 'bb5d58a2-6a2c-48c8-b784-07957e854fbf'
    const taskUpdates: TaskUpdate = {
      title: 'Make a salad',
      description: 'Cesar salad',
    }
    expect(controller.update(id, taskUpdates)).resolves.toBeDefined()
  })

  it('should fail to update one task', () => {
    expect.assertions(1)
    const id = '00000000-0000-0000-0000-000000000000'
    const taskUpdates: TaskUpdate = {
      title: 'Go to sleep early',
      description: 'Do not keep awake',
    }

    expect(controller.update(id, taskUpdates)).rejects.toBeDefined()
  })

  it('should delete a task', () => {
    expect.assertions(1)
    expect(
      controller.delete('bb5d58a2-6a2c-48c8-b784-07957e854fbf'),
    ).resolves.toBeDefined()
  })
})
