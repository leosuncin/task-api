import {
  IsString,
  IsNotEmpty,
  IsDateString,
  IsBoolean,
  IsEnum,
  Matches,
  IsInstance,
} from 'class-validator'

import { TaskPriority } from '../task-priority.enum'

const timeWithTimezoneRegex = /([01]\d|2[0-3]):([0-5]\d):([0-5]\d)(-0\d|-1[0-2]|\+0\d|\+1[0-4]):([0-5]\d)/

export class TaskUpdate {
  @IsString()
  @IsNotEmpty()
  readonly title?: string

  @IsString()
  readonly description?: string

  @IsString()
  @IsDateString()
  readonly date?: Date

  @Matches(timeWithTimezoneRegex)
  readonly from?: string

  @Matches(timeWithTimezoneRegex)
  readonly to?: string

  @IsBoolean()
  readonly allDay?: boolean

  @IsEnum(TaskPriority)
  readonly priority?: TaskPriority

  @IsString()
  @IsNotEmpty()
  readonly location?: string

  @IsInstance(Object)
  readonly metadata?: object
}
