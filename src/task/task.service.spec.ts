import { NotFoundException } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'

import { TaskService } from './task.service'
import { Task } from './task.entity'
import { TaskPriority } from './task-priority.enum'
import { TaskCreate } from './dto/task-create.dto'
import { TaskUpdate } from './dto/task-update.dto'

describe('TaskService', () => {
  let service: TaskService
  const findMock = jest.fn(() => Promise.resolve([]))
  const findOneMock = jest.fn(id =>
    Promise.resolve(
      id !== '00000000-0000-0000-0000-000000000000' ? { id } : null,
    ),
  )
  const saveMock = jest.fn(dto => Promise.resolve(dto))
  const deleteMock = jest.fn(id =>
    Promise.resolve({
      raw: [],
      affected: id !== '00000000-0000-0000-0000-000000000000' ? 1 : 0,
    }),
  )
  const MockRepository = jest.fn().mockImplementation(() => ({
    find: findMock,
    findOne: findOneMock,
    save: saveMock,
    delete: deleteMock,
  }))

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TaskService,
        {
          provide: getRepositoryToken(Task),
          useClass: MockRepository,
        },
      ],
    }).compile()

    service = module.get<TaskService>(TaskService)
  })

  afterEach(() => {
    MockRepository.mockClear()
    findMock.mockClear()
    findOneMock.mockClear()
    saveMock.mockClear()
    deleteMock.mockClear()
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should find all tasks', async () => {
    await service.find()

    expect(findMock).toHaveBeenCalled()
    expect(MockRepository).toHaveBeenCalled()
  })

  it('should create a new task', async () => {
    const newTask: TaskCreate = {
      title: 'Make a sandwich',
      description: '',
      date: new Date(),
      from: '10:00:00-06:00',
      to: '11:00:00-06:00',
      allDay: false,
      priority: TaskPriority.NORMAL,
      location: 'Kitchen',
    }

    await service.create(newTask)

    expect(saveMock).toHaveBeenCalledWith(newTask)
    expect(MockRepository).toHaveBeenCalled()
  })

  it('should show one task', async () => {
    await service.get(1)

    expect(findOneMock).toHaveBeenCalledWith(1)
    expect(MockRepository).toHaveBeenCalled()
  })

  it('should throw NotFoundException', async () => {
    try {
      await service.get('00000000-0000-0000-0000-000000000000')
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException)
    }

    try {
      await service.update('00000000-0000-0000-0000-000000000000', {
        title: 'Not exists',
        description: 'NE',
      })
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException)
      expect(saveMock).not.toBeCalled()
    }
  })

  it('should update a task', async () => {
    const id = '00000000-0000-0000-0000-000000000001'
    const taskUpdates: TaskUpdate = {
      title: 'Make a salad',
      description: 'Cesar salad',
    }
    await service.update(id, taskUpdates)

    expect(findOneMock).toHaveBeenCalledTimes(1)
    expect(saveMock).toHaveBeenCalledWith({ id, ...taskUpdates })
    expect(MockRepository).toHaveBeenCalled()
  })

  it('should delete a task', async () => {
    const id = '00000000-0000-0000-0000-000000000001'
    await service.remove(id)

    expect(deleteMock).toHaveBeenCalledWith(id)
    expect(MockRepository).toHaveBeenCalled()
  })
})
