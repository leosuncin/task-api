export enum TaskPriority {
  URGENT = 'URGENT',
  HIGHEST = 'HIGHEST',
  HIGH = 'HIGH',
  NORMAL = 'NORMAL',
  LOW = 'LOW',
  LOWEST = 'LOWEST',
}
