import { TaskPriority } from './task-priority.enum'
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity()
export class Task {
  @PrimaryGeneratedColumn('uuid', { name: 'task_id' })
  id: string

  @Column()
  title: string

  @Column({ nullable: true })
  description?: string

  @Column('date', { default: () => 'CURRENT_TIMESTAMP' })
  date: Date

  @Column('time with time zone')
  from: string

  @Column('time with time zone')
  to: string

  @Column('boolean', { default: false })
  allDay: boolean

  @Column({ type: 'enum', enum: TaskPriority, default: TaskPriority.NORMAL })
  priority: TaskPriority

  @Column({ nullable: true })
  location?: string

  @Column('jsonb', { default: {} })
  metadata: object
}
