import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, DeleteResult } from 'typeorm'

import { Task } from './task.entity'
import { TaskCreate } from './dto/task-create.dto'
import { TaskUpdate } from './dto/task-update.dto'

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
  ) {}

  find(where?): Promise<Task[]> {
    return this.taskRepository.find(where)
  }

  async get(id): Promise<Task> {
    const task = await this.taskRepository.findOne(id)

    if (!task) {
      throw new NotFoundException(`There isn't any task with id: ${id}`)
    }

    return task
  }

  create(newTask: TaskCreate): Promise<Task> {
    const task = Object.assign(new Task(), newTask)

    return this.taskRepository.save(task)
  }

  async update(id, taskUpdates: TaskUpdate): Promise<Task> {
    const task = await this.taskRepository.findOne(id)

    if (!task) {
      throw new NotFoundException(`There isn't any task with id: ${id}`)
    }

    Object.assign(task, taskUpdates)

    return this.taskRepository.save(task)
  }

  remove(id): Promise<DeleteResult> {
    return this.taskRepository.delete(id)
  }
}
