import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Query,
  Body,
  Param,
  HttpCode,
  HttpStatus,
} from '@nestjs/common'
import { DeleteResult } from 'typeorm'

import { TaskService } from './task.service'
import { Task } from './task.entity'
import { TaskCreate } from './dto/task-create.dto'
import { TaskUpdate } from './dto/task-update.dto'

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get()
  find(@Query() where?): Promise<Task[]> {
    return this.taskService.find(where)
  }

  @Get(':id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})')
  get(@Param('id') id: string): Promise<Task> {
    return this.taskService.get(id)
  }

  @Post()
  create(@Body() newTask: TaskCreate): Promise<Task> {
    return this.taskService.create(newTask)
  }

  @Put(':id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})')
  update(
    @Param('id') id: string,
    @Body() taskUpdates: TaskUpdate,
  ): Promise<Task> {
    return this.taskService.update(id, taskUpdates)
  }

  @Delete(':id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})')
  @HttpCode(HttpStatus.NO_CONTENT)
  delete(@Param('id') id: string): Promise<DeleteResult> {
    return this.taskService.remove(id)
  }
}
