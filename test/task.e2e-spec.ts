import { HttpStatus, INestApplication } from '@nestjs/common'
import { TestingModule, Test } from '@nestjs/testing'
import * as faker from 'faker'
import * as request from 'supertest'

import { AppModule } from '../src/app.module'

function fakeTask() {
  const date = new Date()
  date.setHours(0, 0, 0, 0)
  date.setDate(date.getDate() + 1) // Tomorrow

  return {
    title: faker.hacker.phrase(),
    description: faker.company.catchPhrase(),
    date,
    from: '10:00:00-06:00',
    to: '11:00:00-06:00',
    allDay: false,
    priority: faker.random.arrayElement([
      'URGENT',
      'HIGHEST',
      'HIGH',
      'NORMAL',
      'LOW',
      'LOWEST',
    ]),
    location: faker.address.streetAddress(),
    metadata: {},
  }
}

describe('TaskController (e2e)', () => {
  let app: INestApplication
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()
    faker.seed(3698)

    app = moduleFixture.createNestApplication()
    await app.init()
  })

  afterEach(async () => {
    await app.close()
  })

  it('should list all the tasks', done => {
    request(app.getHttpServer())
      .get('/task')
      .expect(HttpStatus.OK)
      .end(done)
  })

  it('should create a new task', done => {
    request(app.getHttpServer())
      .post('/task')
      .send(fakeTask())
      .expect(HttpStatus.CREATED)
      .end(done)
  })

  it('should get one task', done => {
    request(app.getHttpServer())
      .get('/task/4c4d751c-3d5e-4004-b8b0-017ba0fb54ac')
      .expect(HttpStatus.OK)
      .end(done)
  })

  it('should fail to get one task', done => {
    request(app.getHttpServer())
      .get('/task/00000000-0000-0000-0000-000000000000')
      .expect(HttpStatus.NOT_FOUND)
      .end(done)
  })

  it('should update one task', done => {
    request(app.getHttpServer())
      .put('/task/4c4d751c-3d5e-4004-b8b0-017ba0fb54ac')
      .send(fakeTask())
      .expect(HttpStatus.OK)
      .end(done)
  })

  it('should fail to update one task', done => {
    request(app.getHttpServer())
      .put('/task/00000000-0000-0000-0000-000000000000')
      .send(fakeTask())
      .expect(HttpStatus.NOT_FOUND)
      .end(done)
  })

  it('should delete a task', done => {
    request(app.getHttpServer())
      .post('/task')
      .expect(HttpStatus.CREATED)
      .send(fakeTask())
      .end((err, resp) => {
        if (err) { return done(err) }
        const { body: task } = resp

        request(app.getHttpServer())
          .delete(`/task/${task.id}`)
          .expect(HttpStatus.NO_CONTENT)
          .end(done)
      })
  })
})
