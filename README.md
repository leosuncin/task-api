# Task API

[![pipeline status](https://gitlab.com/leosuncin/task-api/badges/master/pipeline.svg)](https://gitlab.com/leosuncin/task-api/commits/master)
[![coverage report](https://gitlab.com/leosuncin/task-api/badges/master/coverage.svg)](https://gitlab.com/leosuncin/task-api/commits/master)

## Installation

```bash
# Install dependencies
$ npm install

# Copy default environmental variables
$ cp .env.example .env
```

## Database (development)

```bash
# start the database
$ docker-compose up -d

# run migrations (development)
$ yarn typeorm migration:run

# load fixtures
$ yarn fixtures
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm start
```

## Test

```bash
# unit tests
$ npm test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:coverage
```
