const dev = process.env.NODE_ENV !== 'production'

module.exports = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  entities: [dev ? 'src/**/*.entity.ts' : 'dist/src/**/*.js'],
  migrations: [dev ? 'migrations/*.ts' : 'dist/migrations/*.js'],
  cli: {
    migrationsDir: dev ? 'migrations' : 'dist/migrations'
  },
  synchronize: false,
}
