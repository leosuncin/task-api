## 1.0.0 (2019-04-19)

##### Chores

- **task:** Add fixture ([beb48e0f](git+ssh://git@gitlab.com/leosuncin/task-api/commit/beb48e0fe794613e1c4856af740b0a3fddd4a359))
- **package:** Upgrade dependencies ([b7eba754](git+ssh://git@gitlab.com/leosuncin/task-api/commit/b7eba754606540e5a9980ad87b03f68f538ea006))

##### Continuous Integration

- **gitlab:**
  - Update test_e2e job ([8059afe6](git+ssh://git@gitlab.com/leosuncin/task-api/commit/8059afe64c3f42b46f6272fc96495c87177b9dc9))
  - Deploy only with tags ([430bf0bd](git+ssh://git@gitlab.com/leosuncin/task-api/commit/430bf0bd9b6bd34f3d4da53c4f00d207bbb0271a))
  - Deploy using dpl ([3b56cf37](git+ssh://git@gitlab.com/leosuncin/task-api/commit/3b56cf37007eb76f11d0687d4df82ee23c6c13c7))
  - Deploy using SSH key ([219aea21](git+ssh://git@gitlab.com/leosuncin/task-api/commit/219aea21f9bd99d18f2df62816a22be9220ca434))
  - Update jobs ([a7cbab8b](git+ssh://git@gitlab.com/leosuncin/task-api/commit/a7cbab8ba4a65b9be13212339f416f4af6e8f3ed))
- **heroku:**
  - Fix release command ([655885dd](git+ssh://git@gitlab.com/leosuncin/task-api/commit/655885dd39b4a0c33f145fec491a459fd119abe1))
  - Fix release command ([0d7083a9](git+ssh://git@gitlab.com/leosuncin/task-api/commit/0d7083a941ff6102a73d3ab6ffd40f111e16f677))

##### Documentation Changes

- **readme:**
  - Add database instructions ([735e06c8](git+ssh://git@gitlab.com/leosuncin/task-api/commit/735e06c812a2f905e9c2bea046b449a1cc53b214))
  - Add badges from pipeline ([ee4def90](git+ssh://git@gitlab.com/leosuncin/task-api/commit/ee4def90413d77f2fbb5034723e610ec7b5e7317))

##### New Features

- **task:**
  - Add validation annotations to DTOs ([110256ac](git+ssh://git@gitlab.com/leosuncin/task-api/commit/110256ac22a627867169b7a4554914593f7c76a7))
  - Add controller ([984b18fe](git+ssh://git@gitlab.com/leosuncin/task-api/commit/984b18feb6dcb032e46ddf250aa0ce9f8a04e46d))
  - Create DTO classes and use within service ([29cf7bc5](git+ssh://git@gitlab.com/leosuncin/task-api/commit/29cf7bc5b050d2f9ee8cc77899e851ea43c6a82f))
  - Add service ([27eeb984](git+ssh://git@gitlab.com/leosuncin/task-api/commit/27eeb9840ee11a278bdda3173177f5563ac1bc06))
  - Add migration ([1b8f27b1](git+ssh://git@gitlab.com/leosuncin/task-api/commit/1b8f27b1bed04b31b42f78c02c782779a638a3be))
  - Add entity ([56ad9e36](git+ssh://git@gitlab.com/leosuncin/task-api/commit/56ad9e3640d4bc16262ffc4c8831aa0961eeb6b6))
  - Add module ([8087cfe7](git+ssh://git@gitlab.com/leosuncin/task-api/commit/8087cfe7295da76fdbc953c5bf2797f61beba4d1))

##### Tests

- **task:** Add end-to-end tests ([ba711fe4](git+ssh://git@gitlab.com/leosuncin/task-api/commit/ba711fe4438faa0a298f329b7a9217543675fd45))
